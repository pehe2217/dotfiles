" Set the runtime path to include Vundle and initialize
set rtp+=$HOME/dotfiles/vim/bundle/Vundle.vim   " do not use ~
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')
"
" " let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'surround'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
" "filetype plugin on
" 
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just
" :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to
" auto-approve removal
" "
" " see :h vundle for more details or wiki for FAQ
" " Put your non-Plugin stuff after this line


colo wombat256mod
let mapleader = ","

"inoremap " ""<left>
"inoremap ' ''<left>

nnoremap <leader>v :new ~/.vimrc<CR>
nnoremap o o<esc>
nnoremap O O<esc>
nnoremap <c-h> <c-w><c-h>
nnoremap <c-j> <c-w><c-j>
nnoremap <c-k> <c-w><c-k>
nnoremap <c-l> <c-w><c-l>

"Move row-wise even on long lines
nnoremap j gj
nnoremap k gk
nnoremap gj j
nnoremap gk k

"When typing Q in vim, one will go to Ex mode. This is an historical mode for
"backwards-compatability to Vi. The Ex mode seams to be of no use. To
"deactive it:
nnoremap Q <nop>

""incsearch configuration
"map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
let g:incsearch#auto_nohlsearch = 1
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)

filetype plugin indent on
syntax enable

set colorcolumn=80
highlight colorcolumn ctermbg=0 guibg=black

set tabstop=8
set expandtab "expands tabs into spaces
set shiftwidth=4
set softtabstop=4
set autoindent

set splitbelow
set splitright
"set autoindent
set smarttab

set incsearch
set ignorecase
set smartcase "ignore case when searching except if we start with a capital
set number "show line numbers
"set relativenumber "Show line numbers relative to where you are. 
set wildmode=longest,list,full
set t_Co=256

"Set spell language to Great Britain:
set spell spelllang=en_gb
set nospell

"Don't scroll too far
set scrolloff=4

"cd ~/.vim/bundle
"!git clone git://github.com/tpope/vim-sensible.git
"!git clone https://github.com/scrooloose/syntastic.git

let g:syntastic_python_checkers = ['python', 'pep8']

execute pathogen#infect()
let g:syntastic_always_populate_loc_list = 1




"Do yy:<c-r>"<bs><home><c-d><cr>  on the git clone line after pasting below into vimrc


""supertab, adds <Tab> for tab completion
"!git clone git://github.com/ervandew/supertab ~/.vim/bundle/supertab

"" Supertab configuration
"CompletionType = context means that tab will depending on context do e.g.:
"/usr/l<tab>     # will use filename completion
"myvar.t<tab>    # will use user completion if completefunc set,
                "# or omni completion if omnifunc set.
"myvar-><tab>    # same as above
let g:SuperTabDefaultCompletionType = "context"



let g:SuperTabDefaultCompletionTypeDiscovery = [
            \ "&completefunc:<c-x><c-u>",
            \ "&omnifunc:<c-x><c-o>",
            \ ]
let g:SuperTabLongestHighlight = 1

autocmd FileType python set omnifunc=pythoncomplete#Complete
