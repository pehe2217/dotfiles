DIR=$(echo ~/dotfiles)
rm ~/.bashrc
rm ~/.vimrc
rm ~/.vim
rm ~/.tmux.conf
rm ~/.zshrc

ln -s $DIR/bashrc ~/.bashrc
ln -s $DIR/vimrc ~/.vimrc
ln -s $DIR/vim ~/.vim
ln -s $DIR/tmux.conf ~/.tmux.conf
ln -s $DIR/zshrc ~/.zshrc

echo -e "${LBLUE}Copying sunrise-modified-ran.zsh-theme file to ~/.oh-my-zsh/themes/${NC}"
mkdir ~/.oh-my-zsh
mkdir ~/.oh-my-zsh/themes
cp $DIR/oh-my-zsh/themes/sunrise-modified-ran.zsh-theme ~/.oh-my-zsh/themes/.
