"nnoremap <leader>p :!pandoc -s % -o temp_%:r.pdf && xdg-open temp_%:r.pdf &> /dev/null<cr><cr>
nnoremap <leader>p :!./rebuildtwice.sh<cr>
set cc=120
"Linebreak will not break a word into two parts at the end of each line. 
set linebreak
