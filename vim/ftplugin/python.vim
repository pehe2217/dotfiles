nnoremap <leader>p :!clear && python %<CR>
nnoremap <leader>P :!clear && python3 %<CR>
nnoremap <leader>i :!clear && python -i %<CR>
